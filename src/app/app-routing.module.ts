import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule', canLoad: [AuthGuard] },
  // { path: 'beranda', loadChildren: './home/beranda/beranda.module#BerandaPageModule', canLoad: [AuthGuard] },
  // { path: 'riwayat', loadChildren: './home/riwayat/riwayat.module#RiwayatPageModule', canLoad: [AuthGuard] },
  // { path: 'profile', loadChildren: './home/profile/profile.module#ProfilePageModule', canLoad: [AuthGuard] },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
