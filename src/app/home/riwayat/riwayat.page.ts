import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-riwayat',
  templateUrl: './riwayat.page.html',
  styleUrls: ['./riwayat.page.scss'],
})
export class RiwayatPage implements OnInit {
  id: number;
  newuserid: number;
  response: any;

  @Input() searchData;
  theSearchResult = "";
  afterSearch = false;
  beforeSearch = true;
  trxDay: number;
  trxMonth: number;

  constructor(
    private appService: AppService,
    private router: Router,
    private httpClient: HttpClient,
    private alertController: AlertController
    ) { }

  ngOnInit() {
    if(this.appService.isLogin === 0) {
      this.router.navigateByUrl('/auth');
    }
    this.id = this.appService.id;
    this.newuserid = this.appService.id;

    this.httpClient
    .get('https://rano.my.id/receipt-api/transaksi?createdby='+ this.id)
    .subscribe(
      (response:any)  => {
        this.response = response.data;
        console.log(response.data);
    },
    error  => {
      console.log('eror');
      return;
    }
    );


    this.httpClient.get('https://rano.my.id/receipt-api/profilemember?id=' + this.id)
    .subscribe(
      (response:any)  => {
        console.log(response.data);
        this.trxDay = response.data.trx_today;
        this.trxMonth = response.data.trx_monthly;
        if (response.data.trx_today === null) {
          this.trxDay = 0;
        }
        if (response.data.trx_monthly === null) {
          this.trxMonth = 0;
        }
      },
    error  => {
      console.log('err', error);
    }
    );
  }

  onDetail(transaksi) {
    this.appService.setNowTrans(transaksi.id);
    this.router.navigateByUrl('/home/tabs/riwayat/detail');
  }

  onSubmit(searchResult: string) {
    this.searchData = this.response;
    this.theSearchResult = searchResult;
    this.searchData = this.searchData.filter(theData =>
      theData.id
        .toString()
        .toLowerCase()
        .includes(this.theSearchResult.toString().toLowerCase())
    );
    this.showResult();
  }
  showResult() {
    this.afterSearch = true;
    this.beforeSearch = false;
  }

  onSubmitDate(searchResult: string) {
    console.log(searchResult);
  }
  ClearDate() {
    this.beforeSearch = true;
    this.afterSearch = false;
  }

  SearchDate(searchResult: string) {
    this.searchData = this.response;
    this.theSearchResult = searchResult;
    console.log(searchResult);
    this.searchData = this.searchData.filter(theData =>
      theData.trx_date
        .substring(0, 10)
        .toString()
        .toLowerCase()
        .includes(this.theSearchResult.substring(0, 10).toString().toLowerCase())
    );
    this.showResult();
  }
}
