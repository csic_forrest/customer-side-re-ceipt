import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  name = 'Angular 4';
  url = '';
  reader;
  userName: string;
  userImg: string;
  totpoin: number;
  trxDay: number;
  trxMonth: number;

  constructor(
    private appService: AppService,
    private router: Router,
    private alertController: AlertController,
    private httpClient: HttpClient
  ) { }

  ngOnInit() {
    if(this.appService.isLogin === 0) {
      this.router.navigateByUrl('/auth');
    }
    this.userName = this.appService.nameuser;
    const idUser = this.appService.id;
    this.httpClient.get('https://rano.my.id/receipt-api/profilemember?id=' + idUser)
    .subscribe(
      (response:any)  => {
        console.log(response.data);
        this.userImg = this.appService.fotoImg;
        this.trxDay = response.data.trx_today;
        this.trxMonth = response.data.trx_monthly;
        if (response.data.trx_today === null) {
          this.trxDay = 0;
        }
        if (response.data.trx_monthly === null) {
          this.trxMonth = 0;
        }

      },
    error  => {
      console.log('err', error);
    }
    );

    this.httpClient
        .get('https://rano.my.id/receipt-api/point?user_id='+ idUser)
        .subscribe(
            (response:any)  => {
            if(response.status === 204) {
                this.totpoin = 0;
            } else if(response.status === 200) {
                this.totpoin = response.data[0].total_points;
            }
        },
        error  => {
            console.log('eror');
            return;
        }
        );
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.reader = new FileReader();

      this.reader.readAsDataURL(event.target.files[0]);

      this.reader.onload = (event: { target: { result: string; }; }) => {
        this.url = event.target.result;
      };
    }
  }
  public delete() {
    this.url = null;
  }

  async editUserName() {
    const alert = await this.alertController.create({
      header: 'Edit User Name',
      inputs: [
        {
          name: 'username',
          type: 'text',
          id: 'rname-id',
          value: '',
          placeholder: 'Ganti nama...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: (data) => {
            this.changeName(data.username);
          }
        }
      ]
    });

    await alert.present();
  }

  public changeName(userName) {
    console.log(userName);
    this.userName = userName;
    this.appService.setName(userName);
  }

  async editUserImg() {
    const alert = await this.alertController.create({
      header: 'Edit User Photo',
      inputs: [
        {
          name: 'userphoto',
          type: 'text',
          id: 'rphoto-id',
          value: '',
          placeholder: 'Ganti link foto...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: (data) => {
            this.changeImg(data.userphoto);
          }
        }
      ]
    });

    await alert.present();
  }

  
  public changeImg(userImg) {
    console.log(userImg);
    this.userImg = userImg;
    this.appService.setName(userImg);
  }

}
