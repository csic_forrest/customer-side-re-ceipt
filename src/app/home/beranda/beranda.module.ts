import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';

import { BerandaPage } from './beranda.page';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';

const routes: Routes = [
    {
        path: '',
        component: BerandaPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        BarcodeScanner
    ],
    declarations: [BerandaPage]
})
export class BerandaPageModule {}
