import { Component, OnInit } from '@angular/core';
import {ToastController} from "@ionic/angular";
import {BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-beranda',
    templateUrl: './beranda.page.html',
    styleUrls: ['./beranda.page.scss'],
})
export class BerandaPage implements OnInit {
    scannedCode = null;
    nameuser: any;
    restoImg: string;
    id: number;
    trxDay: number;
    trxMonth: number;
    trxYear: number;
    totpoin: number;
    merchantoftheyear: any;
    showmerchantoftheyear: boolean = false;
    lessthree: boolean = true;

    constructor(
        private toastController: ToastController,
        private barcodeScanner: BarcodeScanner,
        private appService: AppService,
        private router: Router,
        private httpClient: HttpClient
    ) { }

    ngOnInit() {
        if(this.appService.isLogin === 0) {
            this.router.navigateByUrl('/auth');
        }
        this.nameuser = this.appService.nameuser;
        this.id = this.appService.id;
        this.restoImg = this.appService.fotoImg;
        console.log(this.nameuser);
        this.httpClient
        .get('https://rano.my.id/receipt-api/homememberrekap?id=' + this.id)
        .subscribe(
            (response:any)  => {
            this.trxDay = response.data.trx_day;
            this.trxMonth = response.data.trx_month;
            this.trxYear = response.data.trx_year;
            if(this.trxDay === null) {
            this.trxDay = 0;
            }
            if(this.trxMonth === null) {
            this.trxMonth = 0;
            }
            if(this.trxYear === null) {
            this.trxYear = 0;
            }
        },
        error  => {
            console.log('eror');
            return;
        }
        );

        this.httpClient
        .get('https://rano.my.id/receipt-api/point?user_id='+ this.id)
        .subscribe(
            (response:any)  => {
            if(response.status === 204) {
                this.totpoin = 0;
            } else if(response.status === 200) {
                this.totpoin = response.data[0].total_points;
            }
        },
        error  => {
            console.log('eror');
            return;
        }
        );


        this.httpClient
        .get('https://rano.my.id/receipt-api/merchantoftheyear?user_id='+ this.id)
        .subscribe(
            (response:any)  => {
            if(response.status === 204) {
                // kalo eror
                console.log('nope');
            } else if(response.status === 200) {
                this.merchantoftheyear = response.data;
                console.log(response.data.length);
                if(this.merchantoftheyear.length <= 3) {
                this.lessthree = true;
                console.log('kurang dari 3');
                } else {
                this.lessthree = false;
                console.log('lebih dari 3');
                this.showmerchantoftheyear = true;
                }
            }
        },
        error  => {
            console.log('eror');
            return;
        }
        );


    }
        showmore() {
            this.showmerchantoftheyear = true;
        }
        showless() {
            this.showmerchantoftheyear = false;
        }

        doScan() {
            this.barcodeScanner.scan(
                {
                    prompt : "", // Android
                }
            ).then(
                barcodeData => {
                    var namemerch;
                    var newemail;
                    var newpassword;
                    var phonenum;
                    this.scannedCode = barcodeData.text;
                    console.log(this.scannedCode)
                    const token = localStorage.getItem("adminToken");
                    const headers = new HttpHeaders().set("authorization", token);
                    
                    // this.httpClient.post('https://rano.my.id/receipt-api/transaksi?email=' + newemail + '&password=' + newpassword,
                    // {
                    //     name : namemerch,
                    //     email : newemail,
                    //     password : newpassword,
                    //     phone_num : phonenum,
                    //     address : location
                    // })
                    // .subscribe(
                    //     (response:any)  => {
                    //         if(response.status === 204) {
                    //             console.log(response.status)
                    //         } else if(response.status === 200) {
                    //             console.log(response.status)
                    //         }
                    //     },
                    //     error  => {
                    //         console.log('gagal');
                    //         return;
                    //     }
                    // );
                    this.httpClient.post('https://rano.my.id/receipt-api/transaksi',
                    {
                        "createdby":this.appService.id,
                        "idx":this.scannedCode
                    })
                    .subscribe(
                        (response:any)  => {
                            if(response.status === 204) {
                                console.log(response.status)
                            } else if(response.status === 200) {
                                console.log(response.status)
                            }
                        },
                        error  => {
                            console.log('gagal');
                            return;
                        }
                    );
                }
            );
        }
}

