import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: HomePage,
        children: [
            {
                path:  'beranda',
                children: [
                    {
                        path: '',
                        loadChildren: './beranda/beranda.module#BerandaPageModule'
                    }
                ]
            },
            {
                path: 'riwayat',
                children: [
                    {
                        path: '',
                        loadChildren: './riwayat/riwayat.module#RiwayatPageModule'
                    }
                ]
            },
            {
                path: 'profile',
                children: [
                    {
                        path: '',
                        loadChildren: './profile/profile.module#ProfilePageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/home/tabs/beranda',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/home/tabs/beranda',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})


export class HomeRoutingModule {}
