import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService:AuthService,
    private router:Router,
    private httpClient:HttpClient,
    private toastController: ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  checkCORS(){
      var url= "http://rano.my.id/receipt-api/pengguna";
      console.log(url);
      this.httpClient.get(url)
        .subscribe(data => {
          console.log(data);
          this.presentToast(data.toString(), 5000);
          // this.status= data.status;
         }, error => {
          console.log(error);
          this.presentToast("error", 5000);
        });
  }

  async presentToast(message: string, duration: number) {
    const toast = await this.toastController.create({
        message: message,
        duration: duration
    });

    await toast.present();
}
}
