import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  constructor(
    private http:HttpClient,
    private router:Router,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    
  }

  onSubmit(form: NgForm){
    console.log(form);

    if(!form.valid){
      return;
    }
    const nama = form.value.nama;
    const email = form.value.email;
    const password = form.value.password;
    const repassword=form.value.repassword;
    const phone = form.value.phone;

    console.log(nama,email,password,repassword,phone);

    var url= "http://rano.my.id/receipt-api/pengguna";

    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'API-KEY':  '8e46d8694293402c2942327098b9f708'
    //     })
    //   };

    var postData={
      "name":nama,
      "email":email,
      "password":password,
      "phone_num":phone,
      "updatedon":""
    }
    this.http.post(url, postData)
      .subscribe((data:any) => {
        console.log(data);
       }, error => {
        console.log(error);
      });
    this.presentToast("Sign Up Success.",1000);
    this.router.navigateByUrl('/auth');
  }

  async presentToast(message: string, duration: number) {
    const toast = await this.toastController.create({
        message: message,
        duration: duration
    });

    await toast.present();
}

}
