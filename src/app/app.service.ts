import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: "root"
})
export class AppService {
  isLogin = 0;
  nameuser: string;
  id: number;
  transid: string;
  fotoImg: string;

  constructor() { }


  getIsLogin() {
    return this.isLogin;
  }
  setIsLogin(isLogin) {
    this.isLogin = isLogin;
  }

  getName() {
    return this.nameuser;
  }
  setName(nameuser: string) {
    this.nameuser = nameuser;
  }

  getID() {
    return this.id;
  }
  setID(id) {
    this.id = id;
  }

  getNowTrans() {
    return this.transid;
  }
  setNowTrans(transid) {
    this.transid = transid;
  }

  getFotoImg() {
    return this.fotoImg;
  }
  setFotoImg(fotoImg) {
    this.fotoImg = fotoImg;
  }

}
