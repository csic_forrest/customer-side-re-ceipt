import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { AppService } from 'src/app/app.service';
import { Router, RouterLink } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _userIsAuthenticated = false;
  private status;
  private thelogin;

  get userIsAuthenticated() {
    return this._userIsAuthenticated;
  }

  constructor(
    private http:HttpClient,
    private router: Router,
    private appService: AppService
  ) { }

  validateUser(email,password){
    var url= "http://rano.my.id/receipt-api/pengguna?email="+email+"&password="+password;
    // console.log(url);
    this.http.get(url)
      .subscribe((data:any) => {
        if(data.status=="200"){
        this.status= data.status;
        this.thelogin = 1;
        this.appService.setName(data.data[0].name);
        this.appService.setID(data.data[0].id);
        this.appService.setFotoImg(data.data[0].img);
        this.appService.setIsLogin(this.thelogin);
          this.login();
        this.router.navigateByUrl('/home/tabs/beranda');
        }
        else{
          console.log("Email atau password salah");
        }
       }, error => {
        console.log(error);
      });
  }

  login() {
    this._userIsAuthenticated = true;
  }

  logout() {
    this._userIsAuthenticated = false;
  }
}
